// ==UserScript==
// @name        WAILT
// @namespace   Violentmonkey Scripts
// @match       https://music.youtube.com/*
// @grant       none
// @version     1.0
// @author      Maxi
// @grant       GM.getValue
// @grant       GM.setValue
// @grant       GM.xmlHttpRequest
// @description Get the currently playing song from youtube music
// ==/UserScript==

(async () => {
  "use strict";

  class CurrentSong {

    static #token;
    static #urlId;

    static get token() {
      return new Promise(async (resolve, reject) => {
        if (this.#token) return resolve(this.#token);

        try {
          let storedKey = GM.getValue("current_song_key");

          // Validate stored API key
          if (storedKey !== undefined) {
            let urlId = await this.#validate(storedKey);
            return resolve(urlId);
          }
        } catch (err) {
          console.error(err);
        }

        // Generate a new API key
        GM.xmlHttpRequest({
          method: "POST",
          url: "http://127.0.0.1:8081/generate",
          onload: res => {
            resolve(JSON.parse(res.responseText));
          },
          onerror: reject
        });
      });
    }

    static #validate(token) {
      return new Promise((resolve, reject) => {
        GM.xmlHttpRequest({
          method: "POST",
          url: "http://127.0.0.1:8081/validate",
          headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
          },
          onload: res => {
            this.#token = token;
            resolve(JSON.parse(res.responseText).id);
          },
          onerror: reject
        });
        return (token === "ilovefillies" ? !!(this.#token = token) : false);
      });
    }

    static #update() {
      GM.xmlHttpRequest({
        method: "POST",
        url: `http://127.0.0.1:8081/update/${this.urlId}`,
        data: JSON.stringify(json),
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${this.#token}`
        },
        onload: res => {
          resolve(true);
        },
        onerror: reject
      });
    }

    static updateSong(el) {
      const title = el.innerText;
      const info = el.parentNode.childNodes[3].innerText;
      const url = new String(location).match(/watch\?v=([0-9a-zA-Z_-]+)/)[1];

      this.#update({
        title: title,
        info: info,
        url: url
      });
    }

    static updateVideo(e) {
      const paused = e.target.paused;
      const currentTime = e.target.currentTime;
      const duration = e.target.duration;
    }
  }

  // Mutation observer config
  const config = {
    attributes: true,
    childList: true,
    characterData: true
  };

  const playerBar = document.querySelector(".content-info-wrapper").childNodes[1];
  const video = document.querySelector("video");

  video.onloadeddata = updateVideo;
  video.onseeked = updateVideo;
  video.onpause = updateVideo;
  video.onstalled = updateVideo;

  const observer = new MutationObserver(mutations => {
    for (const mutation of mutations) {
      if (mutation.type === "attributes") updateSong(mutation.target)
    }
  });

  observer.observe(playerBar, config);

})();

// TODO:
// show user his token
// ability to replace token manually
// customize player
// observe video AND playerbar


