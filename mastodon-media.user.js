// ==UserScript==
// @name        Mastodon Media
// @namespace   Violentmonkey Scripts
// @match       https://baraag.net/web/*
// @grant       GM.setValue
// @grant       GM.getValue
// @version     1.0
// @author      parfait
// @description 9/26/2022, 10:12:44 PM
// ==/UserScript==

(async () => {
  "use strict"

  // Whether to display only posts containing medias
  let mediaOnly = await GM.getValue('mediaOnly', false)

  const togglePosts = (recheck = false) => {
    if (recheck) {
      document.querySelectorAll('article:not(article[style~="opacity:"])').forEach((element) => {
        if (!element.querySelector('.media-gallery, .video-player')) element.classList.add('textpost')
      })
    }

    document.querySelectorAll('.textpost').forEach((element) => element.style.display = mediaOnly ? 'none' : '')
  }

  const load = () => {
    const setting = document.createElement('div')
    setting.className = 'column-settings__row'
    setting.innerHTML = '<div class="setting-toggle"><div class="react-toggle"><div class="react-toggle-track"><div class="react-toggle-track-check"><svg width="14" height="11" viewBox="0 0 14 11"><path d="M11.264 0L5.26 6.004 2.103 2.847 0 4.95l5.26 5.26 8.108-8.107L11.264 0" fill="#fff" fill-rule="evenodd"></path></svg></div><div class="react-toggle-track-x"><svg width="10" height="10" viewBox="0 0 10 10"><path d="M9.9 2.12L7.78 0 4.95 2.828 2.12 0 0 2.12l2.83 2.83L0 7.776 2.123 9.9 4.95 7.07 7.78 9.9 9.9 7.776 7.072 4.95 9.9 2.12" fill="#fff" fill-rule="evenodd"></path></svg></div></div><div class="react-toggle-thumb"></div><input id="setting-toggle-home_timeline-shows-text-posts" class="react-toggle-screenreader-only" type="checkbox" checked=""></div><label for="setting-toggle-home_timeline-shows-text-posts" class="setting-toggle__label"><span>Media only</span></label></div>'

    const mediaToggle = setting.querySelector('.react-toggle')
    mediaToggle.className = mediaOnly ? 'react-toggle react-toggle--checked' : 'react-toggle'

    mediaToggle.addEventListener('click', () => {
      mediaToggle.classList.toggle('react-toggle--checked')
      const media = mediaToggle.classList.contains('react-toggle--checked')
      mediaOnly = media
      GM.setValue('mediaOnly', media)
      togglePosts()
    })

    // Append checkbox to settings list, select the icon because you can't select the button
    // itself reliably :)
    document.querySelector('.fa-sliders').parentNode.parentNode.addEventListener('click', (e) => {
      if (e.target.classList.contains('active')) return
      window.setTimeout(() => {
        document.querySelector('.column-header__collapsible__extra').childNodes[0].appendChild(setting)
      }, 1)
    })

    togglePosts(true)
  }

  // Only hide posts if they're visible
  const onScreen = (target) => {
    setTimeout(() => {
      if (target.tagName === 'ARTICLE' && !target.querySelector('.media-gallery, .video-player')) {
        target.classList.add('textpost')
        if (mediaOnly && target.style.opacity !== '0') target.style.display = 'none'
      }
      if (target.classList.contains('status')) {
        const intersect = new IntersectionObserver((entries) => {
          const post = entries[0].target.parentNode.parentNode.parentNode
          if (!post.querySelector('.media-gallery, .video-player')) {
            post.classList.add('textpost')
            if (mediaOnly && entries[0].isIntersecting) post.style.display = 'none'
          }
        }, { rootMargin: '0px', threshold: [0.05] })
        intersect.observe(target)
      }
    }, 10)
  }

  onload = () => {
    const observer = new MutationObserver((mutationList) => {
      for (const mutation of mutationList) {
        mutation.addedNodes.forEach((target) => {
          // Reassign setting button
          if (target.classList.contains('column-header__wrapper')) load()

          // Listen for new posts
          if (target.tagName === 'ARTICLE' || target.classList.contains('status')) onScreen(target)
        })

      }
    })
    observer.observe(document.querySelector('.columns-area__panels__main'), { attributes: false, childList: true, subtree: true })
    load()
  }
})()